﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    public class MeQueryResult
    {
        public MeResult Me {get; set; }
    }
    public class MeResult
    {
        public string Id { get; set; }
        public List<ServerResult> Servers { get; set; }
    }
}
