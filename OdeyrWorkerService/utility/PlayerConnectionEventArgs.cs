﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Utility
{
    public class PlayerConnectionEventArgs : EventArgs
    {
        public string ActionType { get; set; }
        public string ServerId { get; set; }
    }
}
