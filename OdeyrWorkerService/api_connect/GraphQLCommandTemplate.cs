﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.api_connect
{
    public sealed class GraphQLCommandTemplate
    {
        public class Query
        {
            public static string MyServers() =>
             @$"query {{ me {{ id, servers {{ id, name, slug, address, gamePort, mapPort, game {{ id, name, version, type }}, players {{ id, username, uuid, isOnline }} }} }} }}";

            public static string ServerPlayerHistory() =>
             @$"query {{ me {{ id, servers {{ id, players {{ id, username, uuid, isOnline, operator, banned }} }} }} }}";

            public static string ServerPropertiesList(string serverId) => 
             @$"query {{ me {{ id, servers(where: {{ id: ""{serverId}""}}) {{ id, properties {{ key, value }} }} }} }}";
        }

        public class Mutation
        {
            public static string Login(string email, string password) =>
             @$"mutation {{ login(email: ""{email}"" , password: ""{password}"") {{ id }} }}";

            public static string UpdateCommandResult(string result, string cmdId) =>
             @$"mutation {{ updateCommand (data:{{ result: ""{result}""}}, where: {{ id: ""{cmdId}""}}) {{ id }} }}";

            public static string UpdateGameServerStatus(string status, string serverId) =>
             @$"mutation {{ updateGameServer (data:{{ status: {status.ToUpper()}}}, where: {{ id: ""{serverId}""}}) {{ id }} }}";

            public static string CreateNewPlayerOnGameServer(string uuid, string username, string serverId, bool isOnline, string lastSeen) =>
             @$"mutation {{ updateGameServer (data:{{ players: {{ create: {{ uuid: ""{uuid}"", username: ""{username}"", isOnline: true, lastSeen:""{lastSeen}""}}}}}}, where: {{ id: ""{serverId}""}}) {{ id, players {{ id, username, uuid }} }} }}";

            public static string UpdateGameServerPlayerHistory(string id, string username, string serverId, bool isOnline, string lastSeen) =>
             @$"mutation {{ updateGameServer (data:{{ players: {{ update: {{ where: {{ id:""{id}"" }}, data: {{ isOnline:{isOnline.ToString().ToLower()}, username:""{username}"", lastSeen:""{lastSeen}"" }}}}}}}}, where: {{ id: ""{serverId}""}}) {{ id }} }}";

        }

        public class Subscription
        {
            public static string GameServerCreation(string userId) =>
             @$"subscription {{ gameServer(where: {{ owner: {{id:""{userId}"" }} }}, mutationType: CREATED) {{ node {{ id, name, slug, address, gamePort, mapPort, game {{ id, name, version, type }} }} }} }}";

            public static string CommandOnServerCreation(string serverId) =>
             @$"subscription {{ command (serverId: ""{serverId}"", mutationType: CREATED) {{ node {{ id, commandText }} }} }}";

        }
    }
}
