﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    class UpdateCommandMutationResult
    {
        public UpdateCommand UpdateCommand { get; set; }
    }

    class UpdateCommand
    {
        public string Id { get; set; }

    }
}
