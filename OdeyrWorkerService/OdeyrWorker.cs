using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OdeyrService.datastorage;
using OdeyrService.api_connect;
using OdeyrService.GraphQLResults;
using OdeyrService.Model;

namespace OdeyrService
{
    public class OdeyrWorker : IHostedService, IDisposable
    {
        public readonly ILogger<OdeyrWorker> logger;

        private ApiConnect api;
        private DataStorage storage;
        private SetupAgent setupAgent;

        public OdeyrWorker(ILogger<OdeyrWorker> logger)
        {
            this.logger = logger;
        }

        public void Dispose()
        {
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            Initialization().Wait();

            return Task.CompletedTask;
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            logger.LogInformation("Odeyr service stopped");

            //wait for all servers to stop before quitting
            storage?.StopAllServers();

            return Task.CompletedTask;
        }

        private async Task Initialization()
        {
            logger.LogInformation("Started Odeyr Service");

            storage = new DataStorage(logger);
            api = new ApiConnect(logger, storage, storage.PropertiesGlobal.get("endpoint"));
            setupAgent = new SetupAgent(logger, storage);

            //Login to api
            string userId = await api.Login(storage.userLogin.Email, storage.userLogin.Password);
            storage.userLogin.Id = userId;

            // Synchronize servers with GraphQL
            var onlineServers = await api.GetServerIds();

            // Removal of old servers still present on machine
            var NotIntersectList = storage.Servers.Select(serv => serv.Key)
                .Except(onlineServers.Select(oServ => oServ.Id));

            NotIntersectList.ToList().ForEach(toDelCuid =>
            {
                storage.DeleteOldServer(toDelCuid);
            });

            // Addition of server already present
            onlineServers.Where(s => !storage.Servers.Keys.Contains(s.Id)).ToList().ForEach(x =>
            {
                TypeGame type = (TypeGame)Enum.Parse(typeof(TypeGame), x.Game.Type);
                Version version = new Version(x.Game.Version);
                setupAgent.CreateNewServerAsync(x.Id, x.Slug, x.Name, type, version, x.Address, x.GamePort, x.MapPort).Wait();
            });

            //Sync player lists to servers
            var playerLists = await api.GetGameServerPlayerHistory();
            playerLists.ForEach(x =>
            {
                storage.Servers[x.Id].PlayerCache = x.Players;
                Console.WriteLine("Syncing operators with API...");
                storage.Servers[x.Id].SyncOperator();
                Console.WriteLine("Syncing banned players with API...");
                storage.Servers[x.Id].SyncBannedPlayers();
            });

            storage.Servers.ToList().ForEach(x =>
            {
                api.StartCommandSubscription(x.Value);
                x.Value.CommandSaved += api.OnCommandSaved;
                x.Value.UpdateStatus += api.OnStatusUpdate;
                x.Value.PlayerConnection += api.OnPlayerConnection;
            });

            api.StartServerCreationSubscription(setupAgent, storage.userLogin.Id);
        }
    }
}
