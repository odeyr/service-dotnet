﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Model
{
    class OperatorJson
    {
        public string uuid { get; set; }
        public string name { get; set; }
        public int level { get; set; }
        public bool bypassesPlayerLimit { get; set; }
    }
}
