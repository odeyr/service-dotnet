﻿using GraphQL.Client;
using GraphQL.Client.Http;
using GraphQL;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OdeyrService.datastorage;
using OdeyrService.GraphQLResults;
using OdeyrService.Model;
using GraphQL.Client.Serializer.Newtonsoft;
using OdeyrService.Utility;
using OdeyrService.Abstractions;

namespace OdeyrService.api_connect
{
    public enum ApiRequestType {
        Mutation,
        Query,
        Subscription
    }

    class ApiConnect : IApi
    {
        private GraphQLHttpClient _client;
        readonly ILogger<OdeyrWorker> _logger;
        private DataStorage _storage;

        public ApiConnect(ILogger<OdeyrWorker> logger, DataStorage storage, string endpoint)
        {
            this._logger = logger;
            this._storage = storage;

            endpoint ??= "https://develop.api.odeyr.org";
            _client = new GraphQLHttpClient(o =>
            {
                o.EndPoint = new Uri(endpoint);
                o.UseWebSocketForQueriesAndMutations = false;
                o.OnWebsocketConnected = OnWebSocketConnect;
                o.JsonSerializer = new NewtonsoftJsonSerializer();
            });

            _client.InitializeWebsocketConnection().Wait();

        }
        
        public Task OnWebSocketConnect(GraphQLHttpClient client)
        {
            _logger.LogInformation("Main websocket is open");
            
            return Task.CompletedTask;
        }

        public async Task<List<ServerResult>> GetServerIds()
        {
            var response = await ExecuteQueryOrMutation<MeQueryResult>(GraphQLCommandTemplate.Query.MyServers());

            return response?.Me?.Servers;
        }

        public async Task<List<ServerResult>> GetGameServerPlayerHistory()
        {
            var response = await ExecuteQueryOrMutation<MeQueryResult>(
                GraphQLCommandTemplate.Query.ServerPlayerHistory());

            return response?.Me.Servers;
        }

        public async Task<List<ServerProperty>> GetGameServerProperties(string serverId)
        {
            _logger.LogInformation("Saving Properties to server...");

            var response = await ExecuteQueryOrMutation<MeQueryResult>(
                GraphQLCommandTemplate.Query.ServerPropertiesList(serverId));

            return response?.Me.Servers[0]?.Properties;
        }

        public async Task<string> Login(string email, string password)
        {
            var response = await ExecuteQueryOrMutation<LoginMutationResult>(
                GraphQLCommandTemplate.Mutation.Login(email, password));

            var loginId = response?.Login.Id;
            _logger.LogInformation("Login id : " + loginId);
            return loginId;
        }

        public void UpdateCommandResult(string cmdId, string result)
        {
            ExecuteQueryOrMutation<UpdateCommandMutationResult>(
                GraphQLCommandTemplate.Mutation.UpdateCommandResult(result, cmdId)).Wait();
        }

        public async void AddNewPlayerToServer(string uuid, string username, string serverId, bool isOnline, string lastSeen)
        {
            var response = await ExecuteQueryOrMutation<UpdateGameServerMutationResult>(
                GraphQLCommandTemplate.Mutation.CreateNewPlayerOnGameServer(uuid, username, serverId, isOnline, lastSeen));

            if(response != null)
                UpdateNewPlayerIdInServerCache(response, uuid, serverId);
        }

        private void UpdateNewPlayerIdInServerCache(UpdateGameServerMutationResult response, string uuid, string serverId)
        {
            string newPlayerId = response.UpdateGameServer.players.FirstOrDefault(x => x.uuid == uuid).id;
            _storage.Servers[serverId].PlayerCache.FirstOrDefault(x => x.uuid == uuid).id = newPlayerId;
            _storage.Servers[serverId].PlayersOnline.FirstOrDefault(x => x.uuid == uuid).id = newPlayerId;
        }

        public void UpdatePlayerConnectionStatus(string id, string username, string serverId, bool isOnline, string lastSeen)
        {
            ExecuteQueryOrMutation<UpdateGameServerMutationResult>(
                GraphQLCommandTemplate.Mutation.UpdateGameServerPlayerHistory(id, username, serverId, isOnline, lastSeen)).Wait();
        }

        public void ChangeServerStatus(string status, string id)
        {
            ExecuteQueryOrMutation<UpdateGameServerMutationResult>(
                GraphQLCommandTemplate.Mutation.UpdateGameServerStatus(status, id)).Wait();
        }

        public void StartServerCreationSubscription(SetupAgent agent, string userId)
        {
            GetGraphQLSubscription<ServerCreationResult>(GraphQLCommandTemplate.Subscription.GameServerCreation(userId))
            .Subscribe(response => ServerCreationResHandling(response, agent));
        }

        public void StartCommandSubscription(Server server)
        {
            GetGraphQLSubscription<CommandSubscritionResult>(GraphQLCommandTemplate.Subscription.CommandOnServerCreation(server.CUID))
            .Subscribe(response => CommandResponseHandling(response, server));
        }

        private void CommandResponseHandling(GraphQLResponse<CommandSubscritionResult> response, Server server)
        {
            var cmd = response.Data.Command.Node.CommandText;
            var cmdId = response.Data.Command.Node.Id;
            _logger.LogInformation(@$"""{cmd}"" launched on {server.CUID} ({server.VisualName})");
            switch (cmd)
            {
                case "start":
                    server.SavePropertiesFromList(GetGameServerProperties(server.CUID).Result);
                    server.StartAsync(cmdId);
                    break;
                case "stop":
                    server.StopAsync(cmdId);
                    break;
                case "restart":
                    server.Stop(cmdId);
                    server.SavePropertiesFromList(GetGameServerProperties(server.CUID).Result);
                    server.StartAsync(cmdId);
                    break;
                default:
                    server.SendSpecificCommand(cmd, cmdId);
                    break;
            }
        }

        private void ServerCreationResHandling(GraphQLResponse<ServerCreationResult> response, SetupAgent agent)
        {
            var data = response.Data.GameServer.Node;
            var type = (TypeGame)Enum.Parse(typeof(TypeGame), data.Game.Type);
            var version = new Version(data.Game.Version);

            Task.Run(() =>
            {
                var serverCreated = agent.CreateNewServerAsync(data.Id, data.Slug, data.Name, type, version, data.Address,
                        data.GamePort, data.MapPort).Result;
                
                this.StartCommandSubscription(serverCreated);
            });
            
        }

        private IObservable<GraphQLResponse<T>> GetGraphQLSubscription<T>(string query)
        {
            var request = new GraphQL.GraphQLRequest
            {
                Query = query
            };

            return _client.CreateSubscriptionStream<T>(request);
        }

        private async Task<T> ExecuteQueryOrMutation<T>(string query)
        {
            var request = new GraphQL.GraphQLRequest
            {
                Query = query
            };

            var response = await _client.SendQueryAsync<T>(request);
            if(response.Errors != null)
            {
                foreach (GraphQLError error in response.Errors)
                {
                    _logger.LogError(error.Message);
                }
            }

            return response.Data;
        }


        public void OnCommandSaved(object source, EventArgs args)
        {
            Server s = (Server)source;
            Task.Run(() => UpdateCommandResult(s.LastCmdId, s.LastCmdResult));
        }

        public void OnStatusUpdate(object source, EventArgs args)
        {
            Server s = (Server)source;
            Task.Run(() => ChangeServerStatus(s.Status, s.CUID));
        }

        public void OnPlayerConnection(object source, PlayerConnectionEventArgs args)
        {
            Player p = (Player)source;

            if(args.ActionType == "create")
                Task.Run(() => AddNewPlayerToServer(p.uuid, p.username, args.ServerId, true, DateTime.Now.ToString("yyyy-MM-ddTHH\\:mm\\:ss")));
            else
                Task.Run(() => UpdatePlayerConnectionStatus(p.id, p.username, args.ServerId, p.isOnline, DateTime.Now.ToString("yyyy-MM-ddTHH\\:mm\\:ss")));
        }

    }
    
}
