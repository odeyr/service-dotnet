﻿using Microsoft.Extensions.DependencyInjection;
using OdeyrService.api_connect;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Abstractions
{
    public class DependencyInjectionContainer
    {
        public IServiceProvider Build()
        {
            var container = new ServiceCollection();

            container.AddSingleton<IApi, ApiConnect>();

            return container.BuildServiceProvider();
        }
    }
}
