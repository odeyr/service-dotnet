﻿using OdeyrService.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    public class ServerResult
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Status { get; set; }
        public string Address { get; set; }
        public int GamePort { get; set; }
        public int MapPort { get; set; }
        public Game Game { get; set; }
        public List<Player> Players { get; set; }
        public List<ServerProperty> Properties { get; set; }
    }

}
