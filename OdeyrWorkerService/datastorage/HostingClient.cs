﻿using Microsoft.Extensions.Logging;
using OdeyrService.Utility;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OdeyrService.datastorage
{
    public class HostingClient
    {
        private const string USERNAME = "sshtunnel";
        Process clientSsh;
        private string pathPrivateKey;
        private string remoteAddress;
        private uint remotePort;

        public string Address
        {
            get { return clientSsh.IsRunning() ? $"{remoteAddress}:{remotePort}" : null; }
        }

        public HostingClient(string pathPrivateKey)
        {
            this.pathPrivateKey = pathPrivateKey;
        }

        public void Start(string remoteAddress, uint remotePort, uint localPort)
        {
            this.remoteAddress = remoteAddress;
            this.remotePort = remotePort;

            clientSsh = new Process();
            clientSsh.StartInfo.FileName = "ssh";
            clientSsh.StartInfo.Arguments = $"-i {pathPrivateKey} -R {remotePort}:localhost:{localPort} -N {USERNAME}@{remoteAddress}";
            clientSsh.EnableRaisingEvents = true;

            clientSsh.Start();
        }

        public void Stop()
        {
            clientSsh.Kill();
            this.remotePort = 0;
        }
    }
}
