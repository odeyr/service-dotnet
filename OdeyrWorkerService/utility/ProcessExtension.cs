﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace OdeyrService.Utility
{
    public static class ProcessExtension
    {
        public static bool IsRunning(this Process process)
        {
            if (process == null)
                throw new ArgumentNullException("process");

            try
            {
                Process.GetProcessById(process.Id);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }
    }
}
