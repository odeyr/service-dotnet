﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Utility
{
    class ConsoleOutputDataParsing
    {
        const string USER_CONNECTED = "User Authenticator";
        const string USER_DISCONNECTED = "left the game";
        const string SERVER_STARTUP_FINISHED = "INFO]: Done ";
        const string COMMAND_RESULT = "INFO";

        public static bool CheckPlayerConnected(string data) =>
             data.Contains(USER_CONNECTED);

        public static bool CheckPlayerDisconnected(string data) =>
             data.Contains(USER_DISCONNECTED);

        public static bool CheckIfCommandResult(string data) =>
             data.Contains(COMMAND_RESULT);

        public static bool CheckServerStartupFinished(string data) =>
             data.Contains(SERVER_STARTUP_FINISHED);

        public static bool IsOutputFromPlayer(string data) =>
             data.Split(" ")[3].Contains("<") && data.Split(" ")[3].Contains(">");



    }
}
