﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Model
{
    public class ServerProperty
    {
        public string id;
        public string key { get; set; }
        public string value { get; set; }
        public ServerProperty defaultValue { get; set; }
    }
}
