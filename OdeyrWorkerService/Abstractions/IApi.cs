﻿using OdeyrService.datastorage;
using OdeyrService.GraphQLResults;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace OdeyrService.Abstractions
{
    public interface IApi
    {
        public Task<string> Login(string email, string password);
        public Task<List<ServerResult>> GetServerIds();
        public Task<List<ServerResult>> GetGameServerPlayerHistory();
        public void UpdateCommandResult(string cmdId, string result);
        public void AddNewPlayerToServer(string uuid, string username, string serverId, bool isOnline, string lastSeen);
        public void UpdatePlayerConnectionStatus(string id, string username, string serverId, bool isOnline, string lastSeen);
        public void ChangeServerStatus(string status, string id);
        public void StartServerCreationSubscription(SetupAgent agent, string userId);
        public void StartCommandSubscription(Server server);
    }
}
