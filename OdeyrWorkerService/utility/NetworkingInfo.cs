﻿using System;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Text;

namespace OdeyrService.Utility
{
    public sealed class NetworkingInfo
    {

        public static bool CheckIfPortIsOpen(int port)
        {
            bool isAvailable = true;
            if (port > 65535) throw new ArgumentOutOfRangeException("Port exceeding maximum value: 65535");

            IPGlobalProperties ipGlobalProperties = IPGlobalProperties.GetIPGlobalProperties();
            TcpConnectionInformation[] tcpConnInfoArray = ipGlobalProperties.GetActiveTcpConnections();

            foreach (TcpConnectionInformation tcpi in tcpConnInfoArray)
            {
                if (tcpi.LocalEndPoint.Port == port)
                {
                    isAvailable = false;
                    break;
                }
            }

            return isAvailable;
        }
    }
}
