using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using OdeyrService.GraphQLResults;
using OdeyrService.Model;
using OdeyrService.Utility;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;

namespace OdeyrService.datastorage
{
    /// <summary>
    /// A Server class that is used to represent a server that can be launched.
    /// </summary>
    public class Server
    {
        public const string ODEYR_CONFIG_NAME = "odeyr.properties";
        public const string ODEYR_EXECUTABLE_NAME = "odeyr-server.jar";
        private const int GAME_PORT_DEFAULT = 25565;
        private const int RCON_PORT_DEFAULT = 25575;
        
        private ILogger<OdeyrWorker> logger;


        public string LastCmdId { get; set; }

        public string LastCmdResult { get; set; }

        public HostingClient HostingClientGame { get; private set; }
        public HostingClient HostingClientRcon { get; private set; }

        public delegate void SaveCommandResultEventHandler(object source, EventArgs args);

        public event SaveCommandResultEventHandler CommandSaved;

        public delegate void UpdateServerStatusEventHandler(object source, EventArgs args);

        public event UpdateServerStatusEventHandler UpdateStatus;

        public delegate void OnPlayerConnectionEventHandler(object source, PlayerConnectionEventArgs args);

        public event OnPlayerConnectionEventHandler PlayerConnection;


        public string FolderPath { get; private set; }

        /// <summary>
        /// The minecraft server properties config file
        /// </summary>
        public Properties ServerConfig { get; private set; }
        
        /// <summary>
        /// A file in which you can overwrite an existing Odeyr
        /// config with a new json data object. it must follow the 
        /// type:
        /// { type: 'vanilla', version: '1.15.2', uuid: '0000-00000000-00000000-0000', visual-name:'Server test', status:'Running|Loading|Unloading|Stopped' }
        /// </summary>
        private Properties OdeyrConfig;

        private Process procServer;

        private bool _IsInstalled;

        public List<Player> PlayerCache { get; set; }

        public List<Player> PlayersOnline { get; set; }

        private readonly EventWaitHandle startWaitHandle = new AutoResetEvent(false);
        private readonly EventWaitHandle stopWaitHandle = new AutoResetEvent(false);

        /// <summary>
        /// Proxy function for easy access of the visual name of the server
        /// </summary>
        public string VisualName
        {
            get { return OdeyrConfig.get("visual-name"); }
        }

        /// <summary>
        /// Proxy function for easy access on the CUID of the server
        /// </summary>
        public string CUID
        {
            get { return OdeyrConfig.get("cuid"); }
        }

        /// <summary>
        /// Proxy function for easy access of the status of the server
        /// </summary>
        public string Status
        {
            get { return OdeyrConfig.get("status"); }
        }

        public string GameAddress
        {
            get { return HostingClientGame.Address; }
        }

        public string RconAddress
        {
            get { return HostingClientRcon.Address; }
        }

        private string TunnelSshFileKey
        {
            get { return Path.Combine(Directory.GetCurrentDirectory(), "sshtunnel.pem");  }
        }

        /// <summary>
        /// Creation of an instance from an already created Server folder
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="folderPath"></param>
        public Server(ILogger<OdeyrWorker> logger, string folderPath)
        {
            _IsInstalled = true;
            this.logger = logger;

            FolderPath = folderPath;
            ServerConfig = new Properties(Path.Combine(folderPath, "server.properties"));
            OdeyrConfig = new Properties(Path.Combine(folderPath, ODEYR_CONFIG_NAME));

            HostingClientRcon = new HostingClient(TunnelSshFileKey);
            HostingClientGame = new HostingClient(TunnelSshFileKey);

            PlayersOnline = new List<Player>();
        }

        /// <summary>
        /// Creation of a server from an empty canvas
        /// </summary>
        /// <param name="logger"></param>
        public Server(ILogger<OdeyrWorker> logger, string folderPath, string serverId, string serverName, TypeGame type, Version minecraftVersion, string remoteAddress, int remoteGamePort, int remoteMapPort)
        {
            _IsInstalled = false;
            this.logger = logger;
            FolderPath = folderPath;

            OdeyrConfig = new Properties(Path.Combine(folderPath, ODEYR_CONFIG_NAME));
            OdeyrConfig.set("type", type.ToString().ToLower());
            OdeyrConfig.set("version", $"{minecraftVersion.Major}.{minecraftVersion.Minor}.{minecraftVersion.Build}");

            OdeyrConfig.set("cuid", serverId);
            OdeyrConfig.set("visual-name", serverName);
            OdeyrConfig.set("status", "stopped");
            OdeyrConfig.set("remote-address", remoteAddress);
            OdeyrConfig.set("remote-game-port", remoteGamePort);
            OdeyrConfig.set("remote-map-port", remoteMapPort);
            OdeyrConfig.Save();

            PlayersOnline = new List<Player>();
        }

        /// <summary>
        /// Task to do after creating the server for the first time
        /// </summary>
        public void InitializeAfterCreation()
        {
            ServerConfig = new Properties(Path.Combine(FolderPath, "server.properties"));
            ServerConfig.set("enable-rcon", Boolean.TrueString.ToLower());
            ServerConfig.set("rcon.password", "01234");
            ServerConfig.set("rcon.port", ChooseEmptyPort(RCON_PORT_DEFAULT));
            ServerConfig.set("broadcast-rcon-to-ops", Boolean.FalseString.ToLower());

            ServerConfig.Save();

            _IsInstalled = true;

            HostingClientRcon = new HostingClient(TunnelSshFileKey);
            HostingClientGame = new HostingClient(TunnelSshFileKey);
        }

        /// <summary>
        /// Start a server synchronously, will wait for the completion of the launch process before returning
        /// </summary>
        /// <param name="id">the id of the last command sent</param>
        public void Start(string id = null)
        {
            startWaitHandle.Reset();
            this.StartAsync(id);

            startWaitHandle.WaitOne();
            startWaitHandle.Reset();
        }


        /// <summary>
        /// A function to start asynchronously this minecraft server. Once
        /// it is called the status is set to "Loading" and once the minecraft server is loaded,
        /// it is set to "Running". It associated the process to different output type
        /// </summary>
        /// <param name="id">the id of the last command sent</param>
        public void StartAsync(string id = null)
        {
            if (_IsInstalled)
            {
                ServerConfig.set("server-port", ChooseEmptyPort(Int32.Parse(ServerConfig.get("server-port"))));
                ServerConfig.Save();
            }

            if (id != null)
                updateLastCommandReceived(id);

            procServer = new Process
            {
                StartInfo =
                {
                    FileName = "java",
                    Arguments = $"-Xms1G -Xmx1G -jar {ODEYR_EXECUTABLE_NAME} nogui",
                    WorkingDirectory = FolderPath,
                    CreateNoWindow = false,
                    UseShellExecute = false,
                    RedirectStandardOutput = true
                }
            };

            procServer.OutputDataReceived += ProcServer_Stdout;

            procServer.StartInfo.RedirectStandardError = true;
            procServer.ErrorDataReceived += ProcServer_Stderr;

            procServer.StartInfo.RedirectStandardInput = true;

            procServer.EnableRaisingEvents = true;
            procServer.Exited += ProcServer_Exited;

            procServer.Start();
            procServer.BeginOutputReadLine();
            procServer.BeginErrorReadLine();

            OdeyrConfig.set("status", "loading");
            OdeyrConfig.Save();
            OnStatusUpdate();
            
            if (_IsInstalled)
            {
                HostingClientRcon.Start(OdeyrConfig.get("remote-address"), Convert.ToUInt32(OdeyrConfig.get("remote-map-port")), 8123);
                HostingClientGame.Start(OdeyrConfig.get("remote-address"), Convert.ToUInt32(OdeyrConfig.get("remote-game-port")), Convert.ToUInt32(ServerConfig.get("server-port")));
            }
        }

        private int ChooseEmptyPort(int defaultValue = 25565)
        {
            int portToBeUsed = defaultValue;
            try
            {
                while (!NetworkingInfo.CheckIfPortIsOpen(portToBeUsed))
                    portToBeUsed++;
            }
            catch (ArgumentOutOfRangeException e)
            {
                logger.LogError("No port are available to use, " + e.Message);
            }
            return portToBeUsed;

        }

        public void Stop(string id = null)
        {
            stopWaitHandle.Reset();
            StopAsync(id);

            if (this.Status == "stopped") return;

            stopWaitHandle.WaitOne();
            stopWaitHandle.Reset();
        }

        /**
         * Stop async the process that is currently running if it is not currently stopped
         */
        public void StopAsync(string id = null)
        {
            PlayersOnline.ForEach(x =>
            {
                x.isOnline = false;
                PlayerConnection?.Invoke(x, new PlayerConnectionEventArgs { ActionType = "update", ServerId = this.CUID });
            });
            PlayersOnline.Clear();

            if (id != null)
                updateLastCommandReceived(id);

            if (this.Status == "unloading" || this.Status == "stopped") return;
            if (procServer == null) return;

            StreamWriter inputServerWriter = procServer.StandardInput;
            inputServerWriter.WriteLine("stop");
            inputServerWriter.Close();

            ServerConfig.set("server-port", GAME_PORT_DEFAULT.ToString());
            this.OdeyrConfig.set("status", "unloading");
            this.OdeyrConfig.Save();
            OnStatusUpdate();

            HostingClientRcon.Stop();
            HostingClientGame.Stop();
        }

        /// <summary>
        /// A function to send a specific command to the server.
        /// This checks if it is running
        /// </summary>
        /// <returns>
        /// A boolean value if the command has been sent to server or not
        /// </returns>
        public bool SendSpecificCommand(string command, string id = null)
        {
            if(id!= null)
                updateLastCommandReceived(id);

            if (this.OdeyrConfig == null) return false;

            if (this.Status == "running")
            {
                StreamWriter inputServerWriter = procServer.StandardInput;
                inputServerWriter.WriteLine(command);
                return true;
            }
            
            return false;
        }

        /// <summary>
        /// Is called once the process has a new error output line in his stream stdout
        /// </summary>
        private void ProcServer_Stderr(object sender, DataReceivedEventArgs data)
        {
            if (data.Data != null)
                logger.LogError(data.Data);
        }
        
        /// <summary>
        /// Is called once the process has a new output line in his stream stdout
        /// </summary>
        private void ProcServer_Stdout(object sender, DataReceivedEventArgs data)
        {
            if (data.Data == null) return;
            // Ignores the output received if it was sent by an in-game player
            if (ConsoleOutputDataParsing.IsOutputFromPlayer(data.Data)) return;

            logger.LogInformation(data.Data);

            if (ConsoleOutputDataParsing.CheckServerStartupFinished(data.Data))
                OnServerStartupFinished();

            if (ConsoleOutputDataParsing.CheckIfCommandResult(data.Data))
            {
                if (LastCmdId == null) return;
                OnCommandSaved(data.Data);
            }
            if (ConsoleOutputDataParsing.CheckPlayerConnected(data.Data))
                OnPlayerConnected(data.Data);

            if (ConsoleOutputDataParsing.CheckPlayerDisconnected(data.Data))
                OnPlayerDisconnect(data.Data);

        }

        /// <summary>
        /// This happens once the server has successfully started up
        /// </summary>
        public void OnServerStartupFinished()
        {
            this.OdeyrConfig.set("status", "running");
            this.OdeyrConfig.Save();
            OnStatusUpdate();
            startWaitHandle.Set();
        }

        /// <summary>
        /// This triggers the OnCommandSaved event in ApiConnect
        /// </summary>
        public virtual void OnCommandSaved(string data)
        {
            LastCmdResult += data.Replace("\"", "") + " #!#";
            CommandSaved?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// This triggers the OnPlayerConnection event in ApiConnect
        /// </summary>
        /// <param name="p"></param>
        /// <param name="actionType"></param>
        public void OnPlayerConnected(string data)
        {
            var output = data.Split(" ");
            Player connectedPlayer = new Player { username = output[7], uuid = output[9], isOnline = true };
            Player cachedPlayer = this.PlayerCache.Where(x => x.uuid == output[9]).ToList().FirstOrDefault();

            PlayersOnline.Add(connectedPlayer);
            string actionType = null;
            if (cachedPlayer == null)
            {
                PlayerCache.Add(connectedPlayer);
                actionType = "create";
            }
            else
            {
                connectedPlayer.id = cachedPlayer.id;
                actionType = "update";
            }

            PlayerConnection?.Invoke(connectedPlayer, new PlayerConnectionEventArgs { ActionType = actionType, ServerId = this.CUID });
        }

        /// <summary>
        /// This happens once a player has been disconnected
        /// </summary>
        /// <param name="data"></param>
        private void OnPlayerDisconnect(string data)
        {
            var output = data.Split(" ");
            string username = output[3];


            if (PlayersOnline.Count > 0)
            {
                Player cachedPlayer = this.PlayersOnline.Where(x => x.username == username).ToList().FirstOrDefault();
                this.PlayersOnline.Remove(cachedPlayer);
                cachedPlayer.isOnline = false;
                PlayerConnection?.Invoke(cachedPlayer, new PlayerConnectionEventArgs { ActionType = "update", ServerId = this.CUID });
            }
        }

        /// <summary>
        /// This triggers the OnStatusUpdate event in ApiConnect
        /// </summary>
        public virtual void OnStatusUpdate()
        {
            UpdateStatus?.Invoke(this, EventArgs.Empty);
        }

        /// <summary>
        /// Is called once the process has exited
        /// </summary>
        private void ProcServer_Exited(object sender, EventArgs e)
        {
            this.OdeyrConfig.set("status", "stopped");
            this.OdeyrConfig.Save();
            OnStatusUpdate();
            startWaitHandle.Set();
            stopWaitHandle.Set();
        }

        /// <summary>
        /// Updates the values of the last command received
        /// </summary>
        /// <param name="id"></param>
        private void updateLastCommandReceived(string id)
        {
            if (id != LastCmdId)
            {
                LastCmdId = id;
                LastCmdResult = "";
            }
        }

        /// <summary>
        /// Go accept the EULA agreements
        /// </summary>
        /// <returns>whether the EULA file has been accepted.</returns>
        public bool AcceptEULA()
        {
            if (!File.Exists(Path.Combine(FolderPath, "eula.txt"))) return false;
            Properties eula = new Properties(Path.Combine(FolderPath, "eula.txt"));
            if (eula.get("eula") == null)
                return false;

            eula.set("eula", "true");
            eula.Save();
            return true;
        }

        /// <summary>
        /// This will sync the ops.txt file with the players operator value from the API if changes were made while the server was offline
        /// </summary>
        public void SyncOperator()
        {
            if (!File.Exists(Path.Combine(FolderPath, "ops.json"))) return;

            List<Player> playersFromAPI = PlayerCache.Where(player => player.@operator).ToList();
            List<OperatorJson> playersFromJson = GetDataFromJson<OperatorJson>("ops.json");

            playersFromAPI.ForEach(@operator =>
            {
                var matchingOp = playersFromJson.Where(player => player.uuid == @operator.uuid);
                if (!matchingOp.Any())
                {
                    playersFromJson.Add(new OperatorJson { uuid = @operator.uuid, name = @operator.username, bypassesPlayerLimit = true, level = 4});
                }
            });

            List<OperatorJson> playersToRemove = new List<OperatorJson>();
            playersFromJson.ForEach(playerToUpdate =>
            {
                var matchingPlayer = playersFromAPI.Where(player => player.uuid == playerToUpdate.uuid);
                if (!matchingPlayer.Any())
                {
                    playersToRemove.Add(playerToUpdate);
                }
            });
            playersToRemove.ForEach(p => playersFromJson.Remove(p));

            File.WriteAllText(Path.Combine(FolderPath, "ops.json"), JsonConvert.SerializeObject(playersFromJson, Formatting.Indented));
        }

        public void SyncBannedPlayers()
        {
            if (!File.Exists(Path.Combine(FolderPath, "banned-players.json"))) return;

            List<Player> playersFromAPI = PlayerCache.Where(player => player.banned).ToList();
            List<BannedPlayerJson> playersFromJson = GetDataFromJson<BannedPlayerJson>("banned-players.json");

            playersFromAPI.ForEach(bannedPlayer =>
            {
                var matchingPlayer = playersFromJson.Where(player => player.uuid == bannedPlayer.uuid);
                if (!matchingPlayer.Any())
                {
                    playersFromJson.Add(new BannedPlayerJson { uuid = bannedPlayer.uuid, name = bannedPlayer.username, created = DateTime.Now.ToString(),
                        expires = "forever", reason = "Banned by an operator", source = "Server" });
                }
            });

            List<BannedPlayerJson> playersToRemove = new List<BannedPlayerJson>();
            playersFromJson.ForEach(playerToUpdate =>
            {
                var matchingPlayer = playersFromAPI.Where(player => player.uuid == playerToUpdate.uuid);
                if (!matchingPlayer.Any())
                {
                    playersToRemove.Add(playerToUpdate);
                }
            });
            playersToRemove.ForEach(p => playersFromJson.Remove(p));

            File.WriteAllText(Path.Combine(FolderPath, "banned-players.json"), JsonConvert.SerializeObject(playersFromJson, Formatting.Indented));
        }

        public void SavePropertiesFromList(IEnumerable<ServerProperty> properties)
        {
            ServerConfig = new Properties(Path.Combine(FolderPath, "server.properties"));

            foreach (var property in properties)
            {
                var previousValue = ServerConfig.get(property.key, "");
                ServerConfig.set(property.key, property.value);
                if(previousValue != property.value)
                    logger.LogInformation("Prop \"" + property.key + "\": " + previousValue + " -> " +property.value);
            }

            ServerConfig.Save();
        }

        private List<T> GetDataFromJson<T>(string fileKey)
        {
            string path = Path.Combine(this.FolderPath, fileKey);

            List<T> data;
            using (StreamReader r = new StreamReader(path))
            {
                string json = r.ReadToEnd();
                data = JsonConvert.DeserializeObject<List<T>>(json);
            }
            return data;
        }
    }
}
