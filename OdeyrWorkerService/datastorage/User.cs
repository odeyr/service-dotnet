﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;
using OdeyrService.Utility;

namespace OdeyrService.datastorage
{
    /// <summary>
    /// A class to get login information and to persist them on disk
    /// </summary>
    public class User
    {
        private string key = "dasdBEJGJSd2r4ed";

        private RSACryptoServiceProvider RSA { get; set; }

        /// <value>encrypt the desired password into a SHA512 attributes</value>
        private string encryptedPassword;

        public string Password
        {
            get
            {
                return GetUnencryptData();
            }
            set
            {
                StoreAndEncrypt(value);
            }
        }

        public string Email 
        {
            get
            {
                return propFile.get("email");
            }
            private set
            {
                propFile.set("email", value);
                propFile.Save();
            }
        }

        private Properties propFile;

        public string Id {
            get
            {
                return propFile.get("id");
            }
            set
            {
                propFile.set("id", value);
                propFile.Save();
            }
        }

        public User(Properties connectionFile)
        {
            propFile = connectionFile;
            RSA = new RSACryptoServiceProvider();

            // Check if the user is already authenticated, or it needs to encrypt password
            if (propFile.get("authenticated") == Boolean.TrueString.ToLower())
                encryptedPassword = propFile.get("password");
            else
                storeCredentials(propFile.get("email"), propFile.get("password"));

            propFile.set("authenticated", Boolean.TrueString.ToLower());
            propFile.Save();
        }

        /// <summary>
        /// Encrypt password and store them on disk of a test connection string
        /// </summary>
        /// <param name="email"></param>
        /// <param name="password"></param>
        public void storeCredentials(string email, string password)
        {
            propFile.set("email", email);
            propFile.Save();

            StoreAndEncrypt(password);
        }


        private void StoreAndEncrypt(string password)
        {
            encryptedPassword = AESOperation.EncryptString(key, password);
            propFile.set("password", encryptedPassword);
            propFile.Save();
        }

        private string GetUnencryptData()
        {
            return AESOperation.DecryptString(key, propFile.get("password"));
        }


        private byte[] RSAEncrypt(byte[] DataToEncrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] encryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                

                    //Import the RSA Key information. This only needs
                    //toinclude the public key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Encrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    encryptedData = RSA.Encrypt(DataToEncrypt, DoOAEPPadding);
                
                return encryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        private byte[] RSADecrypt(byte[] DataToDecrypt, RSAParameters RSAKeyInfo, bool DoOAEPPadding)
        {
            try
            {
                byte[] decryptedData;
                //Create a new instance of RSACryptoServiceProvider.
                using (RSACryptoServiceProvider RSA = new RSACryptoServiceProvider())
                {
                    //Import the RSA Key information. This needs
                    //to include the private key information.
                    RSA.ImportParameters(RSAKeyInfo);

                    //Decrypt the passed byte array and specify OAEP padding.  
                    //OAEP padding is only available on Microsoft Windows XP or
                    //later.  
                    
                    decryptedData = RSA.Decrypt(DataToDecrypt, DoOAEPPadding);
                }
                return decryptedData;
            }
            //Catch and display a CryptographicException  
            //to the console.
            catch (CryptographicException e)
            {
                Console.WriteLine(e.ToString());

                return null;
            }
        }
    }
}
