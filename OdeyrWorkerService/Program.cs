using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.AccessControl;
using System.Threading.Tasks;
using System.Security.Principal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.IO;
using System.Runtime.InteropServices;

namespace OdeyrService
{
    public class Program
    {
        public static void Main(string[] args)
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                FileInfo fInfo = new FileInfo("sshtunnel.pem");

                FileSecurity fSecurity = fInfo.GetAccessControl();
                AuthorizationRuleCollection rules = fSecurity.GetAccessRules(true, true, typeof(NTAccount));
                fSecurity.SetAccessRuleProtection(true, false);

                fSecurity.AddAccessRule(new FileSystemAccessRule(Environment.UserName, FileSystemRights.FullControl, AccessControlType.Allow));
                fInfo.SetAccessControl(fSecurity);
            }

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                CreateHostBuilderWin(args).Build().Run();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
                CreateHostBuilderLinux(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilderWin(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<OdeyrWorker>();
                });

        public static IHostBuilder CreateHostBuilderLinux(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSystemd()
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddHostedService<OdeyrWorker>();
                });
    }
}
