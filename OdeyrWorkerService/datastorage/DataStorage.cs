﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Text;

using Microsoft.Extensions.Logging;
using System.Threading;
using System.Net.NetworkInformation;
using OdeyrService.Utility;

namespace OdeyrService.datastorage
{
    /**
     * A class in which it contains servers instance to be able to access data on disk.
     * This module is related to anything that is accessible from a client perspective
     * and will be used inside the API module
     */
    public class DataStorage
    {
        public const string ODEYR_GLOBAL_CONFIG = "odeyr-global.properties";
        private ILogger<OdeyrWorker> logger;

        /// <value>The environment variable used to define the folder location for servers</value>
        public string EnvironmentVariable { get; private set; } = "ODEYR_DATA_LOCATION";
        public string OdeyrPath { get;  }

        /// <value>A dictionary where each server is match with his uuid</value>
        public Dictionary<string, Server> Servers { get; private set; }
        /// <value>The user that will be used to logon</value>
        public User userLogin { get; private set; }
        public Properties PropertiesGlobal { get; }

        public DataStorage(ILogger<OdeyrWorker> logger)
        {
            this.logger = logger;

            OdeyrPath = Environment.GetEnvironmentVariable(EnvironmentVariable);

            PropertiesGlobal = new Properties(Path.Combine(OdeyrPath, ODEYR_GLOBAL_CONFIG));
            userLogin = new User(PropertiesGlobal);

            Servers = new Dictionary<string, Server>();

            Directory.EnumerateDirectories(OdeyrPath, "*", SearchOption.TopDirectoryOnly).ToList().ForEach(dir =>
            {
                if (File.Exists(Path.Combine(dir, Server.ODEYR_CONFIG_NAME)))
                {
                    var server = new Server(logger, Path.Combine(OdeyrPath, dir));
                    this.Servers.Add(server.CUID, server);
                }
            });
        }

        /**
         * Start one server by it's CUID
         */
        public void StartServer(string uuid)
        {
            Server serverToStart = this.Servers.Where(kvp => kvp.Value.CUID == uuid).First().Value;

            serverToStart.StartAsync();
        }

        /**
         * Stop one server by it's CUID
         */
        public void StopServer(string cuid)
        {
            Server serverToStop = this.Servers.Where(kvp => kvp.Value.CUID == cuid).First().Value;

            serverToStop.Stop();
        }

        /**
         * Stop all servers will wait until all servers are closed
         */
        public void StopAllServers()
        {
            //launch the command to stop server
            foreach (Server serv in Servers.Values)
            {
                serv.Stop();
            }

            //check if each servers is stopped before quitting
            foreach (Server serv in Servers.Values)
            {
                while (serv.Status != "stopped")
                {
                    Thread.Sleep(50);
                }
            }
        }

        public void DeleteOldServer(string cuid)
        {
            Server serverToDelete = this.Servers.Where(kvp => kvp.Value.CUID == cuid).First().Value;
            Directory.Delete(serverToDelete.FolderPath, true);
        }
    }
}
