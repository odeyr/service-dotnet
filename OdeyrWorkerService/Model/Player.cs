﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Model
{
    public class Player
    {
        public string id;
        public string username { get; set; }
        public string uuid { get; set; }
        public bool isOnline { get; set; }
        public bool @operator {get;set;} // the @ is added because operator is C# keyword,
                                         // isOperator wouldn't have worked because the property value has to be the same as the one in graphql
        public bool banned { get; set; }
    }
}
