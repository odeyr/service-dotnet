﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;
using OdeyrService.Utility;
using OdeyrService.GraphQLResults;
using OdeyrService.api_connect;
using OdeyrService.Model;

namespace OdeyrService.datastorage
{
    /// <summary>
    /// A class to create download and install multiple things. It creates server which handles creating folder, fetching the server for minecraft.
    /// storing the right info, etc
    /// </summary>
    public class SetupAgent
    {
        private const string BUILD_TOOLS_NAME = "BuildTools.jar";
        private string minecraftSiteUrl = "https://mcversions.net/";

        private DataStorage storage;
        private ILogger<OdeyrWorker> logger;

        public SetupAgent(ILogger<OdeyrWorker> logger, DataStorage storage)
        {
            this.logger = logger;
            this.storage = storage;
        }

        /// <summary>
        /// Create a new Server using some information to be passed by the API
        /// </summary>
        /// <param name="serverId">the server ID on the API</param>
        /// <param name="serverName">Server name to be shown</param>
        /// <param name="serverType">server type</param>
        /// <param name="minecraftVersion">The minecraft server version</param>
        /// <returns></returns>
        public async Task<Server> CreateNewServerAsync(string serverId, string serverSlug, string serverName, TypeGame serverType, Version minecraftVersion, string remoteAddress, int remoteGamePort, int remoteMapPort)
        {
            string pathToServer = Path.Combine(storage.OdeyrPath, $"{serverSlug}");
            Directory.CreateDirectory(pathToServer);
            Server newServer = new Server(logger, pathToServer, serverId, serverName, serverType, minecraftVersion, remoteAddress, remoteGamePort, remoteMapPort);


            switch (serverType)
            {
                case TypeGame.VANILLA:
                    Stream strFile = File.Create(Path.Combine(pathToServer, "odeyr-server.jar"));
                    await ScrapeVanillaMinecraftWebsite(strFile, minecraftVersion);
                    strFile.Close();
                    break;
                case TypeGame.CRAFTBUKKIT:
                    DownloadBuildTools(pathToServer).Wait();
                    InstallServerWithBuildTools(pathToServer, minecraftVersion);
                    RemoveAllOutputButServerCreated(pathToServer, minecraftVersion);
                    InstallMapOnBukkitServer(pathToServer).Wait();
                    break;
            }
            logger.LogInformation("First startup of server to generate file");
            newServer.Start();
            logger.LogInformation("Linking files and accepting EULA");
            newServer.InitializeAfterCreation();
            newServer.AcceptEULA();

            storage.Servers.Add(newServer.CUID, newServer);
            logger.LogInformation("Server " + serverName + " has finished installing");

            return newServer;
        }

        /// <summary>
        /// Scrape mcversion to download the server jar file of the appropriate version
        /// </summary>
        /// <param name="strFile">the stream to copy the jar server into</param>
        /// <param name="minecraftVersion">the minecraft version to be fetch</param>
        private async Task ScrapeVanillaMinecraftWebsite(Stream strFile, Version minecraftVersion)
        {
            HttpClient client = new HttpClient();

            logger.LogInformation("Fetching right server version from mcversions.net...");
            var getPage = await client.GetAsync(minecraftSiteUrl + $"download/{minecraftVersion.Major}.{minecraftVersion.Minor}.{minecraftVersion.Build}");

            var content = getPage.Content;
            string contentStr = await content.ReadAsStringAsync();
            var strBefore = "<h5>Server Jar</h5><a class=\"button\"";
            var strAfter = "download=\"minecraft_server-" + minecraftVersion.Major + "." + minecraftVersion.Minor + "." + minecraftVersion.Build + ".jar\">Download Server Jar</a>";
            string rest = contentStr.getBetween(strBefore, strAfter).Trim();

            var downloadLink = rest.Split("\"")[1];

            logger.LogInformation("Downloading server from mcversions.net...");
            var res = await client.GetAsync(downloadLink);
            logger.LogInformation("Installing Server...");
            await res.Content.CopyToAsync(strFile);
            logger.LogInformation("Installation done!");
        }

        private async Task DownloadBuildTools(string pathToServer)
        {
            var client = new HttpClient();
            logger.LogInformation("Fetching build tools from spigotmc.org...");
            var res = await client.GetAsync("https://hub.spigotmc.org/jenkins/job/BuildTools/lastSuccessfulBuild/artifact/target/BuildTools.jar");

            Stream strFile = File.Create(Path.Combine(pathToServer, BUILD_TOOLS_NAME));
            await res.Content.CopyToAsync(strFile);
            strFile.Close();
        }

        private void InstallServerWithBuildTools(string pathToServer, Version minecraftVersion)
        {
            logger.LogWarning("This could take some time to process, please wait...");
            logger.LogInformation("Starting Installation of server...");
            var buildServer = new Process
            {
                StartInfo =
                {
                    FileName = "java",
                    Arguments =  $"-Xmx1024M -jar {BUILD_TOOLS_NAME} --rev {minecraftVersion.Major}.{minecraftVersion.Minor}.{minecraftVersion.Build} --compile craftbukkit",
                    WorkingDirectory = pathToServer
                }
            };

            buildServer.Start();
            buildServer.WaitForExit();

            logger.LogInformation("Server successfully built");
        }

        private void RemoveAllOutputButServerCreated(string pathToServer, Version minecraftVersion)
        {
            var serverExeName = $"craftbukkit-{minecraftVersion.Major}.{minecraftVersion.Minor}.{minecraftVersion.Build}.jar";
            if (!File.Exists(Path.Combine(pathToServer, serverExeName)))
            {
                //TODO not hardcode those value, get those from pom.xml
                string artifactId = "craftbukkit";
                string version = $"{minecraftVersion.Major}.{minecraftVersion.Minor}-R0.1-SNAPSHOT";
                string packaging = "jar";
                
                string pathFileGenerated = Path.Combine(pathToServer, "CraftBukkit", "target", $"{artifactId}-{version}.{packaging}");
                File.Move(pathFileGenerated, Path.Combine(pathToServer, serverExeName));
            }

            var exceptPaths = new List<string>()
            {
                Server.ODEYR_CONFIG_NAME,
                serverExeName
            };

            DeleteAllExcept(pathToServer, exceptPaths);

            // Renamed file to match the executable
            File.Move(Path.Combine(pathToServer, serverExeName), Path.Combine(pathToServer, Server.ODEYR_EXECUTABLE_NAME));
        }

        private async Task InstallMapOnBukkitServer(string pathToServer)
        {
            string jarName = "Dynmap-3.0-beta-10-spigot.jar";
            DirectoryInfo dir = Directory.CreateDirectory(Path.Combine(pathToServer, "plugins"));
            var client = new HttpClient();
            logger.LogInformation("Fetching dynmap from spigotmc.org...");
            var res = await client.GetAsync($"https://dynmap.us/releases/{jarName}");

            Stream strFile = File.Create(Path.Combine(dir.FullName, jarName));
            await res.Content.CopyToAsync(strFile);
            strFile.Close();
            logger.LogInformation("DynMap installation done");
        }

        private void DeleteAllExcept(string folderPath, List<string> except, bool recursive = true)
        {
            var dir = new DirectoryInfo(folderPath);

            //Delete files excluding the list of file names
            foreach (var fi in dir.GetFiles().Where(n => !except.Contains(n.Name)))
            {
                fi.Delete();
            }

            //Loop sub directories if recursive == true
            if (recursive)
            {
                foreach (var di in dir.GetDirectories())
                {
                    setAttributesNormal(di);
                    di.Delete(true);
                }
            }
        }

        private void setAttributesNormal(DirectoryInfo dir)
        {
            foreach (var subDir in dir.GetDirectories())
                setAttributesNormal(subDir);
            foreach (var file in dir.GetFiles())
            {
                file.Attributes = FileAttributes.Normal;
            }
        }
    }
}
