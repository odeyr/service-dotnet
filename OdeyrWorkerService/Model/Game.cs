﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.Model
{
    public class Game
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Version { get; set; }
        public string Type { get; set; }
    }

    public enum TypeGame
    {
        VANILLA,
        CRAFTBUKKIT
    }

    public enum NameGame
    {
        MINECRAFT
    }
}
