﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    class CommandSubscritionResult
    {
        public CommandResult Command { get; set; }

        public class CommandResult
        {
            public NodeResult Node { get; set; }

            public class NodeResult
            {
                public string Id { get; set; }
                public string CommandText { get; set; }
            }
        }
    }
}
