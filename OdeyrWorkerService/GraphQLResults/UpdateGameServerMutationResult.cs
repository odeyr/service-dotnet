﻿using OdeyrService.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    public class UpdateGameServerMutationResult
    {
        public UpdateGameServerResult UpdateGameServer { get; set; }
        
        public class UpdateGameServerResult
        {
            public string Id { get; set; }

            public List<Player> players { get; set; }
        }
    }
}
