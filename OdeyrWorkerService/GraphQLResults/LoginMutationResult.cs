﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    public class LoginMutationResult
    {
        public LoginResult Login { get; set; }
    }
    public class LoginResult
    {
        public string Id { get; set; }
    }
}
