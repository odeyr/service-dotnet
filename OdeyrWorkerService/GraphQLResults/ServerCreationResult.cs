﻿using System;
using System.Collections.Generic;
using System.Text;

namespace OdeyrService.GraphQLResults
{
    public class ServerCreationResult
    {
        public GameServerResult GameServer { get; set; }

        public class GameServerResult
        {
            public ServerResult Node { get; set; }
        }
    }
}
